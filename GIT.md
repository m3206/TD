__Vous pouvez conserver ce que vous faites dans votre dépôt en créant un fichier readme.md, ou en mettant les commandes dans un fichier__

## Exercices sur GIT

1. Créer un compte sur gitlab.com et github.com. Choisissez un nom "sérieux",
pas trop long, ni stupide et facile à retenir.
1. Notez que vous pouvez utiliser git sans compte sur gitlab, ni github. En
revanche ces dépots, dit distants vous permettent de stocker quelques part et
de manières accessibles vos dépots.
1. les réponses des exerices devront être envoyé sur votre dépôt pour ce
module.
1. Dans votre interface github ou gitlab, créer un nouveau projet 'public' appelé:
M3206. Une fois créer, lisez bien ce qui est écrit, cela vous donne certaines
indications importantes.

Vous pouvez à partir de la créer un fichier README.md pour décrire ce que
contiendra votre dépôt.

1. Sur votre machine locale, clonez votre dépôt.
`git clone https://<user>@gitlab.com/<user>/M3206.git`

Cette commande doit vous demander un mot de passe et crée un répertoire M3206. Qui est
votre dépôt git.

1. Positionnez-vous dans le répertoire nouvellement crée `cd M3206`. Créer un répertoire `td1`
et se positionner dans ce répertoire.

## Expérimenter avec git.

### Exercice 1.
- Que retourne la commande `git status` ?

- Créer un fichier dans le répertoire `td1` appelé `TD1_Git.txt`. Ajouter la ligne suivante:
`Première ligne du fichier` dans ce fichier. 

- Que retourne la commande `git status` ?

- Vous venez de faire une modification dans votre repertoire de travail. Ajouter 
cette modification dans l'index avec `git add TD1_Git.txt`

- Que retourne la commande `git status` ?

- Vous venez d'ajouter vos modification dans l'index. Maintenant il faut le publier (`commit`).
Saissez la commande: `git commit -m "<MEssage expliquant ce que fait le commit>"`

- Que retourne la commande `git status` ? 

- Votre modifications est publiée, il faut maintenant l'envoyé sur le dépôt distant (dans notre cas
gitlab ou github): `git push origin master`. Cette commande vous demandera votre mot de passe github/gitlab.

- Que retourne la commande `git status` ?

- Si vous allez sur l'interface web de gitlab/github, vous devez voir votre répertoire `td1` 
et le fichier `TD1_Git.txt`.


### Exercice 2.
- Ajouter une deuxième ligne à votre fichier `TD1_Git.txt`, ajouter (git add) les modifications à l'index, et
publier votre modification (git commit). 

- Ajouter une troisième ligne à votre fichier `TD1_Git.txt`, ajouter (git add) les modifications à l'index, et
publier votre modification (git commit). 

- Ajouter une quatrième ligne à votre fichier `TD1_Git.txt`, ajouter (git add) les modifications à l'index, et
publier votre modification (git commit). 

- Attention, pour le moment nous n'envoyons rien sur le dépot distant. Attention mettez des messages explicites 
pour chaque commit.

- Que retourne la commande `git log`

### Exercice 3.

- Ajouter une cinquième ligne à votre fichier `TD1_Git.txt`.

- Que retourne la commande `git status` ?

- Imaginons maintenant que cette cinquième ligne ne vous plaise pas. Pour revenir à la version précédente:
tapez: `git checkout -- TD1_Git.txt`. 

- Vérifier que la 5ieme ligne a disparu.

- Que retourne la commande `git status` ?

 
### Exercice 4.

- Ajouter encore une fois une cinquième ligne à votre fichier `TD1_Git.txt`.

- Ajouter les modifications à l'index (git add) ?

- Que retourne la commande `git status` ?

- une fois ajouté à l'index, vous pouvez encore changer d'avis. 
Pour revenir à la version précédente: tapez: `git reset HEAD TD1_Git.txt` 
qui permet d'enlever le fichier de l'index et  `git checkout -- TD1_Git.txt` 
(comme précedemment) pour revenir à la version précédente.

- Vérifier que la 5ieme ligne a disparu.

- Que retourne la commande `git status` ?

### Exercice 5

- Votre fichier doit maintenant contenir 4 lignes. Les 4 lignes sont publiées.

- `git log` vous permet de voir la liste de vos commits.

- Pour revenir à la version 3 lignes de votre fichier, vous pouvez tapez:
`git revert HEAD~1..HEAD`. En faisant cela on crée un commit qui enlève le dernier commit.
git vous demande dans ce cas de mettre un message expliquant ce retour.

- Vérifier que votre fichier ne contient plus que 3 lignes.


### Exercice 6

- Envoyer les modifications sur le dépôt distant: `git push`

### Exercice 7

- Créer une branche `TEST` avec la commande: `git branch TEST`
- Positionnez-vous sur cette branche: `git checkout TEST`
- Modifier le fichier `TD1_Git.txt` en ajoutant une ligne: "LIGNE: Ajouté depuis la branche TEST"
- Ajouter (git add) cette modification à l'index, et publier la (git commit).
- Revenez sur la branche par défaut (master): `git checkout master`
- Que contient le fichier `TD1_Git.txt` ? 
- Nous allons maintenant inclure les modifications faîtes dans la branche TEST 
à notre branche master (branche par défaut): `git merge TEST`
- Que contient votre fichier `TD1_Git.txt` ? 
- Que retourne la commande `git log` ?
- Envoyer les modifications sur votre dépot distant (git push).